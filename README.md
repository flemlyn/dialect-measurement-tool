# dialect-measurement-tool

A flexible tool to measure the degree of dialectality in layman dialect collections (= Laiendialektsammlungen)

The dialect measurement tool is a highly flexible and configurable software which enables its users to process different layman dialect collections.
Because these types of collections are in general not phonetically transcribed using IPA, SAMPA etc. the tool helps you to transcribe the input data. The component
called `phonetic transcription converter` can be configured by providing two configuration files: a token rule file and a transcription rule file. Each word
of the collection must be tokenized in order to process it correctly, e.g. the german diphthong `ei` should be considered as one phonetic segment. Therefore it has
to be transformed into a single token. Otherwise it would be processed as two separate tokens. The rules of the transcription rule files are then applied on the 
tokenized words. The result of this process is a phonetically transcribed dialect word.

The phonetic transcriptions are used to perform the dialect measurements. The standard german comparative data is automatically fetched by the tool. It uses
the Openthesaurus synonyms database to find all synonyms of a dialect word's meanings. All available meanings (the original one provided in the dialect collection
 and the fetched synonyms) are compared against the dialect word. Because the dialectality measurings have to be executed on the phonetic
transcriptions of the standard german words, their IPA transcriptions are queried automatically from the german Wiktionary database.
The dialect measurings are based on the Levensthein distance measuring algorithm. 

## Source code

The source code of the software is located in `src/flemlyn`

## Requirements

### Dependencies

Some dependencies are available via pip, see `requirements.txt`.

To install the dependencies run:

```
pip install -r requirements.txt
```

---

The project uses `phonecodes` to convert between SAMPA and IPA, see [phonecodes](https://github.com/jhasegaw/phonecodes) on github.
The source code of this project is currently included in `src/phonecodes`. Consider that this may be an outdated version!


### Openthesaurus database

You have to install a MariaDB instance (or MySql) and create a database to import the openthesaurus database dump.

The connection via python is done with the [mariadb python connector](https://mariadb.com/resources/blog/mariadb-connector-python-beta-now-available/)

On Ubuntu based systems you have to install some additional libs before running the pip command:

```
$ sudo apt-get install libmariadbclient-dev

$ pip3 install --pre mariadb
```

Create a database (like openthesaurus) and user (like db_user) in MariaDB and import the thesaurus database dump: 

```
mariadb -u db_user -p openthesaurus < openthesaurus_dump.sql
```

The openthesaurus database dump can be downloaded from the [website](https://www.openthesaurus.de/about/download)
