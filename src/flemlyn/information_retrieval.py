"""

"""
import mariadb
import configparser
from wiktionaryparser import WiktionaryParser
from cache import WiktionaryCache

class InformationRetrievalManager:
    """The InformationRetrievalManager is responsible to provide the standard german measuring data.
    """

    def __init__(self):
        self.__config_parser = configparser.ConfigParser();
        self.__wiktionary_parser = WiktionaryParser();
        self.__wiktionary_parser.set_default_language('german');
        self.__db_user_name = None;
        self.__db_user_password = None;
        self.__db_host = None;
        self.__db_port = None;
        self.__db_database = None;
        self.__opened_connection = None;
        self.__wiktionary_cache = WiktionaryCache();
        self.__words_ipa_not_found = set();

    def get_words_ipa_not_found(self) -> set:
        return self.__words_ipa_not_found;

    def check_database_config(self, config: configparser.SectionProxy) -> bool:
        """Check the given database config for missing parameters."""
        if 'user' not in config:
            print('The parameter \'user\' is missing.');
            return False;
        if 'password' not in config:
            print('The parameter \'password\' is missing.');
            return False;
        if 'host' not in config:
            print('The parameter \'host\' is missing.');
            return False;
        if 'port' not in config:
            print('The parameter \'port\' is missing.');
            return False;
        if 'database' not in config:
            print('The parameter \'database\' is missing.');
            return False;

        return True;

    def load_db_config(self, filename: str) -> bool:
        """Load the given database config.

        If a parameter is missing in the config file the configuration can
        not be loaded.

        :param filename: the name of the db config file
        :type filename: str
        :raise ConfigLoadingError: This exception indicates that the section 'database' is missing
        :raise MissingSectionHeaderError: This exception indicates that there is no section in the config file

        """
        self.__config_parser.read(filename);

        if 'database' in self.__config_parser:
            database_config = self.__config_parser['database'];

            if self.check_database_config(database_config):
                self.__db_user_name = database_config['user'];
                self.__db_user_password = database_config['password'];
                self.__db_host = database_config['host'];
                self.__db_port = database_config['port'];
                self.__db_database = database_config['database'];
                return True;
            else:
                return False;
        else:
            raise ConfigLoadingError('The config file with name' + filename + ' does not contain a [database] section.');

    def open_database_connection(self):
        """Open a database connection using the parameters of the loaded db config file.

        :raise ConfigLoadingError: This exception indicates that a missing/an invalid db config file has been loaded.
        """
        if not self.__db_user_name or not self.__db_user_password or not self.__db_host or not self.__db_port or not self.__db_database:
            raise ConfigLoadingError("No (valid) db configuration has been loaded.");

        try:
            connection = mariadb.connect(
                user=self.__db_user_name,
                password=self.__db_user_password,
                host= self.__db_host,
                port= int(self.__db_port),
                database=self.__db_database
            )
        except mariadb.Error as e:
            print(f"Error connecting to MariaDB Platform: {e}")
            return;

        if connection:
            print('DB is open.')
        self.__opened_connection = connection;

    def close_database_connection(self):
        if self.__opened_connection:
            self.__opened_connection.close();
            print('DB is closed');

    def __format_queried_data_as_list(self, queried_data: list) -> list:
        """Format the queried data as list containing the words.

        The queried data is returned as a list of tuples representing a database row.
        """
        formatted_data = [];

        for row in queried_data:
            formatted_data.append(row[0]);

        return formatted_data;

    def query_synonyms_for_word(self, word: str) -> list:
        """Query all synonyms for the given word."""
        if self.__opened_connection:
            cursor = self.__opened_connection.cursor();
        else:
            raise DatabaseConnectionError("No database connection has been established.");

        cursor.execute("SELECT t.word FROM term t, synset, term term2 WHERE synset.is_visible = 1 AND synset.id = t.synset_id AND term2.synset_id = synset.id AND term2.word = ?", (word,));

        queried_data = cursor.fetchall();

        return self.__format_queried_data_as_list(queried_data);

    def __parse_wiktionary_ipa_result(self, result: str) -> str:
        """"""
        ##TODO: delete
        print(result);
        if not result:
            return '';

        result = result.replace("ˈ", '');

        if '/' in result:
            ipa_word = result.split('/');
            return ipa_word[1];
        if '[' in result:
            ipa_word = result.split('[');
            ipa_word = ipa_word[1].split(']');
            return ipa_word[0];
        elif result:
            return result;
        else:
            return '';

    def __remember_word_with_ipa_data_not_found(self, word: str):
        self.__words_ipa_not_found.add(word);

    def clear_remembered_words_with_ipa_data_not_found(self):
        self.__words_ipa_not_found.clear();

    def get_ipa_data_for_standard_german_word(self, word: str) -> str:
        """Get the IPA transcription of the given word."""

        if not word:
            return '';

        if self.__wiktionary_cache.is_element_in_cache(word):
            return self.__wiktionary_cache.get_value_from_cache(word);
        else:
            print('Fetch word <' + word + '> from wiktionary.');
            wiki_content = self.__wiktionary_parser.fetch(word);

            if wiki_content and 'pronunciations' in wiki_content[0] and 'text' in wiki_content[0]['pronunciations'] and wiki_content[0]['pronunciations']['text']:
                ipa_word = wiki_content[0]['pronunciations']['text'][0];

                self.__wiktionary_parser.session.close();
                ipa_word = self.__parse_wiktionary_ipa_result(ipa_word);
                self.__wiktionary_cache.add_element_to_cache(word, ipa_word);
                return ipa_word;
            else:
                self.__wiktionary_parser.session.close();
                self.__remember_word_with_ipa_data_not_found(word);
                return '';

class ConfigLoadingError(Exception):
    """"""
    pass;

class DatabaseConnectionError(Exception):
    """"""
    pass;