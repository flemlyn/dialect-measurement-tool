"""This module provides the functionality of the tokenization process

Classes
-------

Token - represents a token

TokenRule - represents a token rule

Tokenizer - handles the tokenization process

NoTokenRuleFoundError - indicates that no token rule can be found when trying to tokenize a word
"""
import logging
from typing import List, Tuple


class Token:
    """A token represents a string value which can contain more than one character

    :ivar __value: the value of the token
    """

    def __init__(self, value: str = ""):
        self.__value = value;

    def get_value(self):
        return self.__value;

    def set_value(self, value):
        self.__value = value;


class TokenRule:
    """This class represents a token rule

    Token rules are used by the Tokenizer during the tokenization process.
    The rules determine what tokens are created.

    :ivar __rule_value: the value of the rule used for the tokenization process
    :ivar __is_marked: not yet implemented
    """

    def __init__(self, rule_value: str, is_marked: bool = False):
        """Construct a token rule

        :param rule_value: the rule's value
        :type rule_value: str
        :param is_marked: indicates that the rule needs some manual interaction
        :type is_marked: bool
        """
        self.__rule_value = rule_value;
        self.__is_marked = is_marked;

    def get_rule_value(self):
        return self.__rule_value;


# Type Aliases
TokenList = List[Token];
TokenRuleTuple = Tuple[TokenRule];
TokenTuple = Tuple[Token];


class Tokenizer:
    """The Tokenizer is responsible for the tokenization process

    On construction the tokenizer loads a file containing token rules.
    Each rule represents a specific token, e.g. 'aa' or 'au'.
    These rules are used to tokenize a word.

    :ivar __rule_file_path: the path of the rule file
    :ivar __available_token_rules: the token rules which are available for the tokenizer

    Methods
    -------

    get_available_token_rules() -> TokenRuleTuple
    tokenize_word(word: str) -> TokenList
    """

    def __init__(self, rule_file_path: str):
        """Construct a new tokenizer which uses the rules provided by the given rule file

        On construction the tokenizer tries to load the rules from the given rule file

        :param rule_file_path: the path of the rule file
        :type rule_file_path: str
        """
        self.__rule_file_path = rule_file_path;
        self.__available_token_rules = self.__load_tokenization_rules_from_file(rule_file_path);

    def __load_tokenization_rules_from_file(self, rule_file_path: str) -> TokenRuleTuple:
        """Load the tokenization rules from the given file

        The rules are sorted by their length.

        :param rule_file_path: the path of the rule file
        :type rule_file_path: str
        :return: a tuple containing all loaded rules
        :rtype: TokenRuleTuple
        """
        with open(rule_file_path) as rule_file:
            loaded_rules = [];
            for line in rule_file:
                if line.startswith("#") or line.isspace():
                    continue;
                if line.startswith("!"):
                    line = line.strip({" ", "!"});
                    line = line.rstrip();
                    loaded_rules.append(TokenRule(line, True));
                else:
                    line = line.strip();
                    line = line.rstrip();
                    loaded_rules.append(TokenRule(line, False));

            self.__sort_loaded_rules(loaded_rules);

            return tuple(loaded_rules);

    def __sort_loaded_rules(self, loaded_rules: list):
        """Sort the list of loaded rules by their length

        The longest rules are the first elements in the list.

        :param loaded_rules: the list of loaded rules
        :type loaded_rules: list
        """
        loaded_rules.sort(key=lambda x: len(x.get_rule_value()), reverse=True);

    def get_available_token_rules(self) -> TokenRuleTuple:
        return self.__available_token_rules;

    def print_available_token_rules(self):
        """Print the loaded token rules"""
        for rule in self.__available_token_rules:
            print(rule.get_rule_value());

    def tokenize_word(self, word: str) -> TokenList:
        """Tokenize a given word

        The word is tokenized using the token rules of the Tokenizer. The word is
        lower cased before processing.

        :param word: the word to tokenize
        :type word: str
        :return: a list of tokens representing the given word
        :rtype: TokenList
        """
        #TODO: Mit einem flag in der klasse kann man bestimmen, ob man manuelle hilfe beim tokenizen zulassen möchte oder nicht; tritt ein fehler auf, weil die regel mit einem ! gekennzeichnet, wird an das manuelle programm übergeben

        index = 0;
        tokenized_word = [];
        is_rule_found = False;

        word = word.lower();

        while index < len(word):
            for rule in self.__available_token_rules:
                if word.startswith(rule.get_rule_value(), index):
                    logging.info("Apply rule: " + rule.get_rule_value());
                    index = index + len(rule.get_rule_value());
                    is_rule_found = True;
                    tokenized_word.append(Token(rule.get_rule_value()));
                    break;

            if is_rule_found:
                is_rule_found = False;
            else:
                raise NoTokenRuleFoundError("A tokenization rule is missing for: " + word[index:]);

        return tokenized_word;


# probleme für den tokenizer: diphthong ei, doppelter laut zeigt quantität (dehnung), zb aarschli, autagglat,

# ei ist kandidat für !ei

class NoTokenRuleFoundError(Exception):
    pass;