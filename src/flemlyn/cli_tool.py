import argparse
import csv
import sys
import time
from tokens import TokenList, Tokenizer, NoTokenRuleFoundError
from ruleutils import RuleManager, RuleExecutor, RuleLoadingError
from pt_converter import PhoneticTranscriptionConverter
from distance_measuring import DistanceMeasuringManager
from information_retrieval import InformationRetrievalManager

def __set_delimiter(delimiter: str):
    """Set the global delimiter to the given value if it exists"""
    if delimiter:
        global csv_delimiter;
        csv_delimiter = delimiter[0];


def __tokenize_word_with(tokenizer: Tokenizer, word: str) -> TokenList:
    """"""
    return tokenizer.tokenize_word(word);



def __print_tokenized_word(tokenized_word: TokenList):
    """"""
    printable_word = '';
    for token in tokenized_word:
        printable_word = printable_word + token.get_value() + '  ';
    print(printable_word);

def __convert_token_word_to_sampa(tokenized_word: TokenList) -> TokenList:
    """"""
    return rule_executor.apply_transformation_rules_on_input(tokenized_word);

def __convert_input_word_to_ipa(tokenizer: Tokenizer, word: str) -> str:
    """"""
    try:
        tokenized_word = __tokenize_word_with(tokenizer, word);
    except NoTokenRuleFoundError as e:
        occured_errors.append(e);
        return;

    if args.verbose:
        __print_tokenized_word(tokenized_word);

    if(args.tokenize_only):
        return;

    transformed_word = __convert_token_word_to_sampa(tokenized_word);

    if args.verbose:
        print('----');
        __print_tokenized_word(transformed_word);
        print('----');

    if args.sampa_only:
        return;

    ipa_token_word = phonetic_trans_converter.transform_sampa_to_ipa(transformed_word);
    return phonetic_trans_converter.convert_ipa_token_list_to_string(ipa_token_word);

def __write_word_to_output_file(word: str):
    """"""
    if not word:
        return;

    with open(args.output_file[0], "a") as output_file:
        output_file.write(word + '\n');

def __print_error_messages():
    """"""
    if occured_errors:
        print('\n######## ERROR ########\n');
        for error in occured_errors:
            print(error);
        print('\n#######################\n');

def __print_ipa_results(ipa_result_list: list):
    print('####### IPA and meanings #######');

    for row in ipa_result_list:
        meanings = ''.join([str(elem)+',' for elem in row[1:]]);
        meanings = meanings.rstrip(',');
        print(row[0] + ',' + meanings);

    print('###################');

def __write_measuring_result_to_file(filename: str, result: list):
    with open(filename, 'a') as result_file:
        result_file.write(str(result) + '\n');


def __add_arguments_to_parser(parser: argparse.ArgumentParser):
    """"""
    parser.add_argument('token_rule_file', type=str, nargs=1, help='the path of the token rule file');
    parser.add_argument('transcription_rule_file', type=str, nargs=1, help='the path of the transcription rule file');
    parser.add_argument('input_file', type=str, nargs=1, help='the path of the input_file');
    parser.add_argument('-o', '--output_file', type=str, nargs=1, help='the path of the output file for the IPA transcription of the words');
    parser.add_argument('-d', '--delimiter', type=str, nargs=1, help='the delimiter used in the csv file. The default is a semicolon');
    parser.add_argument('-t', '--tokenize-only', action='store_true', help='Stop after tokenization');
    parser.add_argument('-s', '--sampa-only', action='store_true', help='Stop after SAMPA transcription');
    parser.add_argument('-v', '--verbose', action='store_true', help='Print verbose information');
    parser.add_argument('-l', '--levenshtein', action='store_true', help='Calculate the Levenshtein distance');

########################
parser = argparse.ArgumentParser(description="Phonetic Transcription Converter CLI");

__add_arguments_to_parser(parser);

args = parser.parse_args();
csv_delimiter = ';';
occured_errors = [];
default_db_config_file = 'db_config.ini';
#print(args);

__set_delimiter(args.delimiter);

tokenizer = Tokenizer(args.token_rule_file[0]);
rule_manager = RuleManager(args.transcription_rule_file[0]);

try:
    rule_manager.load_rules_from_file(args.transcription_rule_file[0]);
except RuleLoadingError as e:
    occured_errors.append(e);

rule_executor = RuleExecutor(rule_manager);
phonetic_trans_converter = PhoneticTranscriptionConverter();

with open(args.input_file[0], "r") as collection_file:
    read_collection_csv = csv.reader(collection_file, delimiter=csv_delimiter);

    ipa_word_list = [];
    for row in read_collection_csv:
        result_word = __convert_input_word_to_ipa(tokenizer, row[0]);

        if result_word:
            copy_row = row;
            copy_row[0] = result_word;
            ipa_word_list.append(copy_row);

        if result_word and not args.output_file:
            print(result_word);

        if args.output_file:
            __write_word_to_output_file(result_word);

        if args.verbose:
            print();

if args.verbose:
    __print_ipa_results(ipa_word_list);

if args.levenshtein:
    print('\n##### Starting distance measuring #####\n');
    info_retrieval_manager = InformationRetrievalManager();
    info_retrieval_manager.load_db_config(default_db_config_file);
    info_retrieval_manager.open_database_connection();
    measuring_manager = DistanceMeasuringManager(info_retrieval_manager);
    result_filename = 'distance_measure_result_' + time.strftime("%Y-%m-%d-%H:%M:%S") + '.txt';

    for ipa_word_tuple in ipa_word_list:
        result_list = measuring_manager.get_distance_measuring_result(ipa_word_tuple);
        print('Distance measure result: ' + str(result_list));
        __write_measuring_result_to_file(result_filename, result_list);

    info_retrieval_manager.close_database_connection();
    print('\n###### No IPA data found for words #####');
    print(info_retrieval_manager.get_words_ipa_not_found());
    print('########################################')

###########################
__print_error_messages();