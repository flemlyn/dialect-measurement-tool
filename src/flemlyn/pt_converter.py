"""Phonetic Transcription Converter

The Phonetic Transcription Converter is a rule-based converter that converts any character to an IPA symbol.

The installation of the chardet module is required.
"""
from tokens import TokenList, Token
from io import TextIOWrapper
from chardet.universaldetector import UniversalDetector
from phonecodes.src import phonecodes
import codecs

class PhoneticTranscriptionConverter:
    """"""

    def transform_sampa_to_ipa(self, input: TokenList) -> TokenList:
        """Transform the given sampa tokens to ipa tokens"""

        ipa_token_list = [];

        for token in input:
            ipa_token_list.append(Token(phonecodes.xsampa2ipa(token.get_value(), "deu")));

        return ipa_token_list;

    def convert_ipa_token_list_to_string(self, token_list: TokenList) -> str:
        """Convert the IPA token list to a string"""

        ipa_string = "";

        for token in token_list:
            ipa_string = ipa_string + token.get_value();

        return ipa_string;


class InputChecker:
    """"""

    def __init__(self):
        pass;

    def list_all_available_characters(self, input_file):
        raise Exception("Not implemented yet");

    def is_file_utf8_encoded(self, input_file_path: str) -> bool:
        """Check if the given file is utf8 encoded

        The chardet module is used to detect the file encoding.
        Consider that the character detection is not 100% reliable.

        :return: true if the file's encoding is utf-8 or ascii
        """
        file = open(input_file_path, "rb");
        detector = UniversalDetector();

        for line in file.readlines():
            detector.feed(line);
            if detector.done: break;

        detector.close();
        file.close()

        if detector.result["encoding"] == "utf-8" or detector.result["encoding"] == "ascii":
            return True;
        else:
            return False;

    def convert_file_to_utf8(self, input_file_path: str) -> TextIOWrapper:
        """Convert a given file to utf-8"""
        with codecs.open(input_file_path, 'r') as file_for_conversion:
            read_file_for_conversion = file_for_conversion.read();
            file_for_conversion.close();
        with codecs.open(input_file_path, 'w', 'utf-8') as converted_file:
            converted_file.write(read_file_for_conversion)
            converted_file.close();
            return converted_file;