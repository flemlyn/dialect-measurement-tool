import unittest
import tempfile
import os
from src.flemlyn.ruleutils import *

class RuleUtilsTest(unittest.TestCase):

    def test_load_rules_from_file(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"a;b\nc;d\nee;f");
        tmp_rule_file.flush();

        try:
            self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
            rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
            rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN

            self.assertNotEqual(0, len(rule_manager.get_rulesC0_list()));

        finally:
            tmp_rule_file.close();

if __name__ == '__main__':
    unittest.main()
