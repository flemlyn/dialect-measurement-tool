import unittest
import tempfile
from pt_converter import InputChecker, PhoneticTranscriptionConverter
from tokens import Token


class InputCheckerIsFileUtf8Test(unittest.TestCase):

    def test_is_file_utf8_encoded_with_utf8_file_get_true(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"\xea\x80\x80abcd\xde\xb4");
        tmp_rule_file.flush();

        input_checker = InputChecker();

        # WHEN
        result = input_checker.is_file_utf8_encoded(tmp_rule_file.name);

        # THEN
        self.assertTrue(result);

    def test_is_file_utf8_encoded_with_ascii_file_get_true(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"abcd");
        tmp_rule_file.flush();

        input_checker = InputChecker();

        # WHEN
        result = input_checker.is_file_utf8_encoded(tmp_rule_file.name);

        # THEN
        self.assertTrue(result);


class PhoneticTranscriptionConverterConvertSampaToIpaTest(unittest.TestCase):

    def test_transform_sampa_to_ipa(self):
        # GIVEN
        sampa_token_list = [Token("C"), Token("R"), Token("Y")];
        converter = PhoneticTranscriptionConverter();

        # WHEN
        ipa_token_list = converter.transform_sampa_to_ipa(sampa_token_list);

        # THEN
        self.assertEqual(3, len(ipa_token_list));
        self.assertEqual("\u00E7", ipa_token_list[0].get_value());
        self.assertEqual("\u0281", ipa_token_list[1].get_value());
        self.assertEqual("\u028F", ipa_token_list[2].get_value());

    def test_convert_ipa_token_list_to_string(self):
        # GIVEN
        sampa_token_list = [Token("C"), Token("R"), Token("Y")];
        converter = PhoneticTranscriptionConverter();
        ipa_token_list = converter.transform_sampa_to_ipa(sampa_token_list);

        # WHEN
        ipa_string = converter.convert_ipa_token_list_to_string(ipa_token_list);

        # THEN
        self.assertNotEqual("", ipa_string);
        self.assertTrue(isinstance(ipa_string, str));
        self.assertEqual("\u00E7\u0281\u028F", ipa_string);

if __name__ == '__main__':
    unittest.main()
