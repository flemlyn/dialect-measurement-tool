import unittest
import tempfile
import os
import configparser
from information_retrieval import *

def create_valid_test_db_config(self):
    tmp_config_file = tempfile.NamedTemporaryFile();
    tmp_config_file.write(b"[database]\nuser = diplomarbeit\npassword = diplomarbeit\nhost = 127.0.0.1\nport = 3306\ndatabase = openthesaurus");
    tmp_config_file.flush();
    return tmp_config_file;

class InfoRetrievalManagerCheckDBConfigTest(unittest.TestCase):

    def test_check_database_config_with_valid_config_get_true(self):
        # GIVEN
        tmp_config_file = create_valid_test_db_config(self);
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(True, result);

    def test_check_database_config_with_config_missing_user_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\n");
        tmp_config_file.flush();
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(False, result);

    def test_check_database_config_with_config_missing_password_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\nuser = diplomarbeit");
        tmp_config_file.flush();
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(False, result);

    def test_check_database_config_with_config_missing_host_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\nuser = diplomarbeit\npassword = diplomarbeit");
        tmp_config_file.flush();
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(False, result);

    def test_check_database_config_with_config_missing_port_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\nuser = diplomarbeit\npassword = diplomarbeit\nhost = 127.0.0.1");
        tmp_config_file.flush();
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(False, result);

    def test_check_database_config_with_config_missing_database_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\nuser = diplomarbeit\npassword = diplomarbeit\nhost = 127.0.0.1\nport = 3306");
        tmp_config_file.flush();
        self.assertEqual(False, os.stat(tmp_config_file.name).st_size == 0);

        config_parser = configparser.ConfigParser();
        config_parser.read(tmp_config_file.name);
        self.assertNotEqual([], config_parser.sections());

        database_config = config_parser['database'];
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.check_database_config(database_config);

        # THEN
        self.assertEqual(False, result);


class InfoRetrievalManagerLoadDBConfigTest(unittest.TestCase):

    def test_load_db_config_with_valid_config_get_true(self):
        # GIVEN
        tmp_config_file = create_valid_test_db_config(self);
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.load_db_config(tmp_config_file.name);

        # THEN
        self.assertEqual(True, result);

    def test_load_db_config_with_invalid_config_get_false(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[database]\npassword = diplomarbeit\nhost = 127.0.0.1\nport = 3306\ndatabase = openthesaurus");
        tmp_config_file.flush();
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.load_db_config(tmp_config_file.name);

        # THEN
        self.assertEqual(False, result);

    def test_load_db_config_with_wrong_section_get_error(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"[data]\nuser = diplomarbeit\npassword = diplomarbeit\nhost = 127.0.0.1\nport = 3306\ndatabase = openthesaurus");
        tmp_config_file.flush();
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        with self.assertRaises(ConfigLoadingError) as context:
            info_retrieval_manager.load_db_config(tmp_config_file.name);

        # THEN

    def test_load_db_config_with_missing_section_get_error(self):
        # GIVEN
        tmp_config_file = tempfile.NamedTemporaryFile();
        tmp_config_file.write(b"user = diplomarbeit\npassword = diplomarbeit\nhost = 127.0.0.1\nport = 3306\ndatabase = openthesaurus");
        tmp_config_file.flush();
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        with self.assertRaises(configparser.MissingSectionHeaderError) as context:
            info_retrieval_manager.load_db_config(tmp_config_file.name);

        # THEN


## This is not a unittest. It requires an existing database
class InfoRetrievalManagerConnectTest(unittest.TestCase):

    def test_open_database_connection(self):
        tmp_config_file = create_valid_test_db_config(self);
        info_retrieval_manager = InformationRetrievalManager();

        if info_retrieval_manager.load_db_config(tmp_config_file.name):
            cursor = info_retrieval_manager.open_database_connection();
            print(info_retrieval_manager.query_synonyms_for_word('bank'));


class InfoRetrievalManagerWiktionaryTest(unittest.TestCase):

    def test_get_ipa_data_for_standard_german_word(self):
        # GIVEN
        info_retrieval_manager = InformationRetrievalManager();

        # WHEN
        result = info_retrieval_manager.get_ipa_data_for_standard_german_word('sollen');

        # THEN
        self.assertEqual('zɔlən', result);