import unittest
from distance_measuring import *

class DistanceMeasuringManagerDifferingWordsList(unittest.TestCase):

    def test_get_least_differentiating_words_in_list_with_empty_word_get_empty_list(self):
        # GIVEN 
        comp_word = None;
        word_list = ['Bank', 'Sitzbank', 'Geldhaus'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([], result_list);

    def test_get_least_differentiating_words_in_list_with_empty_list_get_empty_list(self):
        # GIVEN
        comp_word = 'bank';
        word_list = [];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([], result_list);

    def test_get_least_differentiating_words_in_list_with_list_size_1_get_one_word(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Sitzbank'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([('Sitzbank', 4)], result_list);

    def test_get_least_differentiating_words_in_list_with_list_size_3_get_least_differing_word(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Bank', 'Sitzbank', 'Geldhaus'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([('Bank', 1)], result_list);

    def test_get_least_differentiating_words_in_list_with_list_size_3_get_least_differing_words(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Bank', 'Sitzbank', 'Geldhaus', 'Tank'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([('Bank', 1), ('Tank', 1)], result_list);

    def test_get_least_differentiating_words_in_list_with_list_size_3_get_same_word(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Bank', 'Bank', 'bank'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_least_differentiating_words_in_list(comp_word, word_list);

        # THEN
        self.assertEqual([('bank', 0)], result_list);


class DistanceMeasuringManagerWordsLessThanValue(unittest.TestCase):

    def test_get_words_differing_less_than_value_0_get_list(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Bank', 'Bank', 'bank'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_words_differing_less_than_value(0, comp_word, word_list);

        # THEN
        self.assertEqual([('bank', 0)], result_list);

    def test_get_words_differing_less_than_value_3_get_list(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Sitzbank', 'Bank', 'bank', 'Geldhaus'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_words_differing_less_than_value(3, comp_word, word_list);

        # THEN
        self.assertEqual([('bank', 0)], result_list);

    def test_get_words_differing_less_than_value_1_get_list(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Sitzbank', 'Bank', 'Tank', 'Geldhaus'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_words_differing_less_than_value(1, comp_word, word_list);

        # THEN
        self.assertEqual([('Bank', 1), ('Tank', 1)], result_list);

    def test_get_words_differing_less_than_value_0_get_empty_list(self):
        # GIVEN
        comp_word = 'bank';
        word_list = ['Sitzbank', 'Bank', 'Bank', 'Geldhaus'];
        dist_manager = DistanceMeasuringManager();

        # WHEN
        result_list = dist_manager.get_words_differing_less_than_value(0, comp_word, word_list);

        # THEN
        self.assertEqual([], result_list);
