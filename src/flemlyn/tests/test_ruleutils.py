import unittest
import tempfile
import os
from ruleutils import *


class RuleManagerLoadRulesTest(unittest.TestCase):

    def test_load_rules_from_file_with_commented_rules_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\n#a;b\n#c;d\n#e;f");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(0, len(rule_manager.get_rulesC0_list()));

    def test_load_rules_from_file_with_valid_rules_get_rule_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na;b\nc;d\ne;f");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(3, len(rule_manager.get_rulesC0_list()));
        self.assertTrue(isinstance(rule_manager.get_rulesC0_list()[0], RuleC0));
        self.assertTrue(isinstance(rule_manager.get_rulesC0_list()[0].get_premise_value(), Token));
        self.assertEqual("a", rule_manager.get_rulesC0_list()[0].get_premise_value().get_value());
        self.assertEqual("b", rule_manager.get_rulesC0_list()[0].get_conclusion_value().get_value());
        self.assertEqual("c", rule_manager.get_rulesC0_list()[1].get_premise_value().get_value());
        self.assertEqual("d", rule_manager.get_rulesC0_list()[1].get_conclusion_value().get_value());
        self.assertEqual("e", rule_manager.get_rulesC0_list()[2].get_premise_value().get_value());
        self.assertEqual("f", rule_manager.get_rulesC0_list()[2].get_conclusion_value().get_value());

    def test_load_rules_from_file_with_invalid_rules_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the invalid rules\na a a a;b");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        with self.assertRaises(RuleLoadingError) as context:
            rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(0, len(rule_manager.get_rulesC0_list()));
        self.assertEqual(0, len(rule_manager.get_rulesC1_list()));
        self.assertEqual(0, len(rule_manager.get_rulesC2_list()));

    def test_load_rules_from_file_with_valid_c1_rules_get_rule_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na b;a c\nc d;c e\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(0, len(rule_manager.get_rulesC0_list()));
        self.assertEqual(2, len(rule_manager.get_rulesC1_list()));
        self.assertTrue(isinstance(rule_manager.get_rulesC1_list()[0], RuleC1));
        self.assertTrue(isinstance(rule_manager.get_rulesC1_list()[0].get_premise_value(), Token));
        self.assertEqual("b", rule_manager.get_rulesC1_list()[0].get_premise_value().get_value());
        self.assertEqual("a", rule_manager.get_rulesC1_list()[0].get_context_before().get_value());
        self.assertEqual("c", rule_manager.get_rulesC1_list()[0].get_conclusion_value().get_value());
        self.assertEqual("d", rule_manager.get_rulesC1_list()[1].get_premise_value().get_value());
        self.assertEqual("c", rule_manager.get_rulesC1_list()[1].get_context_before().get_value());
        self.assertEqual("e", rule_manager.get_rulesC1_list()[1].get_conclusion_value().get_value());

    def test_load_rules_from_file_with_invalid_c1_rules_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na b;a c \nc d;c e\nf g ;f h");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        with self.assertRaises(RuleLoadingError) as context:
            rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(1, len(rule_manager.get_rulesC1_list()));

    def test_load_rules_from_file_with_valid_c2_rules_get_rule_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na b c;a d c\ne f g;e h g\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(0, len(rule_manager.get_rulesC0_list()));
        self.assertEqual(0, len(rule_manager.get_rulesC1_list()));
        self.assertEqual(2, len(rule_manager.get_rulesC2_list()));
        self.assertTrue(isinstance(rule_manager.get_rulesC2_list()[0], RuleC2));
        self.assertTrue(isinstance(rule_manager.get_rulesC2_list()[0].get_premise_value(), Token));
        self.assertEqual("b", rule_manager.get_rulesC2_list()[0].get_premise_value().get_value());
        self.assertEqual("a", rule_manager.get_rulesC2_list()[0].get_context_before().get_value());
        self.assertEqual("c", rule_manager.get_rulesC2_list()[0].get_context_after().get_value());
        self.assertEqual("d", rule_manager.get_rulesC2_list()[0].get_conclusion_value().get_value());
        self.assertEqual("f", rule_manager.get_rulesC2_list()[1].get_premise_value().get_value());
        self.assertEqual("e", rule_manager.get_rulesC2_list()[1].get_context_before().get_value());
        self.assertEqual("g", rule_manager.get_rulesC2_list()[1].get_context_after().get_value());
        self.assertEqual("h", rule_manager.get_rulesC2_list()[1].get_conclusion_value().get_value());

    def test_load_rules_from_file_with_invalid_c2_rules_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na b c ;a d c\ne f  g;e h g\na b c; a d c\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        with self.assertRaises(RuleLoadingError) as context:
            rule_manager.load_rules_from_file(tmp_rule_file.name);

        # THEN
        self.assertEqual(0, len(rule_manager.get_rulesC0_list()));
        self.assertEqual(0, len(rule_manager.get_rulesC1_list()));
        self.assertEqual(0, len(rule_manager.get_rulesC2_list()));


class RuleManagerPrintRulesOfClassTest(unittest.TestCase):

    def test_print_rules_of_class_before_rule_file_loaded_get_output(self):
        # GIVEN
        class_identifier = 0;
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na;b\nc;d\ne;f\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        output = rule_manager.print_rules_of_class(class_identifier);

        # THEN
        self.assertEqual("No rules to print\n", output);

    def test_print_rules_of_class_with_invalid_identifier_get_error(self):
        # GIVEN
        class_identifier = 3;
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na;b\nc;d\ne;f\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        with self.assertRaises(ClassNotFoundError) as context:
            output = rule_manager.print_rules_of_class(class_identifier);

        # THEN

    def test_print_rules_of_class_0_with_rules_get_formatted_output(self):
        # GIVEN
        class_identifier = 0;
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na;b\nc;d\ne;f\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        output = rule_manager.print_rules_of_class(class_identifier);

        # THEN
        self.assertEqual("class 0: premise: (a) -- conclusion: (b)\nclass 0: premise: (c) -- conclusion: (d)\nclass 0: premise: (e) -- conclusion: (f)\n", output);

    def test_print_rules_of_class_1_with_rules_get_formatted_output(self):
        # GIVEN
        class_identifier = 1;
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\na b;a c\nd e;d f\ng h;g i\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        output = rule_manager.print_rules_of_class(class_identifier);

        # THEN
        self.assertEqual("class 1: premise: (a b) -- conclusion: (a c)\nclass 1: premise: (d e) -- conclusion: (d f)\nclass 1: premise: (g h) -- conclusion: (g i)\n", output);

    def test_print_rules_of_class_2_with_rules_get_formatted_output(self):
        # GIVEN
        class_identifier = 2;
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee c;a f c\na g cc;a h cc\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        output = rule_manager.print_rules_of_class(class_identifier);

        # THEN
        self.assertEqual("class 2: premise: (aa b c) -- conclusion: (aa d c)\nclass 2: premise: (a ee c) -- conclusion: (a f c)\nclass 2: premise: (a g cc) -- conclusion: (a h cc)\n", output);


class RuleManagerPrintAllRulesTest(unittest.TestCase):

    def test_print_all_rules_before_rules_loaded_get_output(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee c;a f c\na g cc;a h cc\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        output = rule_manager.print_all_rules();

        # THEN
        self.assertEqual("No rules to print\n", output);

    def test_print_all_rules_with_all_rule_classes_get_formatted_output(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        output = rule_manager.print_all_rules();

        # THEN
        self.assertEqual("class 0: premise: (a) -- conclusion: (b)\nclass 1: premise: (a ee) -- conclusion: (a f)\nclass 2: premise: (aa b c) -- conclusion: (aa d c)\n", output);


class RuleManagerCheckIfRuleExistsTest(unittest.TestCase):

    def test_check_if_rule_already_exists_with_invalid_class_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        rule_to_check = Rule((Token("a"), ), (Token("b"), ));

        # WHEN
        with self.assertRaises(ClassNotFoundError) as context:
            rule_manager.check_if_rule_already_exists(rule_to_check);

        # THEN


    def test_check_if_rule_already_exists_before_rules_loaded_get_false(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        rule_to_check = RuleC0((Token("a"), ), (Token("b"), ));

        # WHEN
        result = rule_manager.check_if_rule_already_exists(rule_to_check);

        # THEN
        self.assertFalse(result);

    def test_check_if_rule_already_exists_with_not_existing_rules_get_false(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        rule_c0_to_check = RuleC0((Token("aa"), ), (Token("bb"), ));
        rule_c1_to_check = RuleC1((Token("aa"), Token("eee")), (Token("aa"), Token("f")));
        rule_c2_to_check = RuleC2((Token("aaa"), Token("b"), Token("c")), (Token("aaa"), Token("d"), Token("c")));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result_c0 = rule_manager.check_if_rule_already_exists(rule_c0_to_check);
        result_c1 = rule_manager.check_if_rule_already_exists(rule_c1_to_check);
        result_c2 = rule_manager.check_if_rule_already_exists(rule_c2_to_check);

        # THEN
        self.assertFalse(result_c0);
        self.assertFalse(result_c1);
        self.assertFalse(result_c2);

    def test_check_if_rule_already_exists_with_existing_rules_get_true(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        rule_c0_to_check = RuleC0((Token("a"), ), (Token("b"), ));
        rule_c1_to_check = RuleC1((Token("a"), Token("ee")), (Token("a"), Token("f")));
        rule_c2_to_check = RuleC2((Token("aa"), Token("b"), Token("c")), (Token("aa"), Token("d"), Token("c")));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result_c0 = rule_manager.check_if_rule_already_exists(rule_c0_to_check);
        result_c1 = rule_manager.check_if_rule_already_exists(rule_c1_to_check);
        result_c2 = rule_manager.check_if_rule_already_exists(rule_c2_to_check);

        # THEN
        self.assertTrue(result_c0);
        self.assertTrue(result_c1);
        self.assertTrue(result_c2);


class RuleManagerGetAllRulesWithPremiseValueTest(unittest.TestCase):

    def test_get_all_rules_with_premise_value_with_rules_not_loaded_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("x");

        # WHEN
        result = rule_manager.get_all_rules_with_premise_value(premise_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_premise_value_not_existing_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("x");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_premise_value(premise_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_premise_value_existing_get_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;c\nz b;z c");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("b");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_premise_value(premise_value);

        # THEN
        self.assertEqual(3, len(result));









class RuleManagerGetAllRulesWithConclusionValueTest(unittest.TestCase):

    def test_get_all_rules_with_conclusion_value_with_rules_not_loaded_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        conclusion_value = Token("x");

        # WHEN
        result = rule_manager.get_all_rules_with_conclusion_value(conclusion_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_conclusion_value_not_existing_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\n");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        conclusion_value = Token("x");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_conclusion_value(conclusion_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_conclusion_value_existing_get_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        conclusion_value = Token("d");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_conclusion_value(conclusion_value);

        # THEN
        self.assertEqual(3, len(result));


class RuleManagerGetAllRulesWithPremiseValueC0(unittest.TestCase):

    def test_get_all_rules_with_premise_value_from_list_c0_before_rules_loaded_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("a");

        # WHEN
        result = rule_manager.get_all_rules_with_premise_value_from_list_c0(premise_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_premise_value_from_list_c0_with_no_rules_get_empty_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("x");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_premise_value_from_list_c0(premise_value);

        # THEN
        self.assertEqual(0, len(result));

    def test_get_all_rules_with_premise_value_from_list_c0_with_rules_get_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);

        premise_value = Token("a");

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_manager.get_all_rules_with_premise_value_from_list_c0(premise_value);

        # THEN
        self.assertEqual(2, len(result));


class RuleExecutorApplyRulesOnInputTest(unittest.TestCase):

    def test_apply_transformation_rules_on_input_with_empty_word_get_none(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = ();

        # WHEN
        result = rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN
        self.assertIsNone(result);

    def test_apply_transformation_rules_on_input_with_no_token_word_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = "noToken";

        # WHEN
        with self.assertRaises(InputNotTokenizedError) as context:
            rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN

    def test_apply_transformation_rules_on_input_before_rules_loaded_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = (Token("a"), Token("a"), Token("a"));

        # WHEN
        with self.assertRaises(NoRulesFoundError) as context:
            rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN

    def test_apply_transformation_rules_on_input_with_no_rule_for_first_char_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = (Token("x"), Token("a"), Token("a"));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        with self.assertRaises(NoRulesFoundError) as context:
            rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN

    def test_apply_transformation_rules_on_input_with_two_rules_for_first_char_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa b c;aa d c\na ee;a f\na;b\nb;d\nz b;z d\na;d");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = (Token("a"), Token("a"), Token("a"));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        with self.assertRaises(MultipleRulesFoundError) as context:
            rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN

    def test_apply_transformation_rules_on_input_with_rules_for_all_chars_get_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\nb a a;b x a\na a;a z\na;b\nb;d\na i e;a x e\nb i;b x\ni;x");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = (Token("b"), Token("a"), Token("a"), Token("i"), Token("a"));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN
        self.assertNotEqual(0, len(result));
        self.assertEqual(5, len(result));
        self.assertEqual("d", result[0].get_value());
        self.assertEqual("x", result[1].get_value());
        self.assertEqual("z", result[2].get_value());
        self.assertEqual("x", result[3].get_value());
        self.assertEqual("b", result[4].get_value());

    def test_apply_transformation_rules_on_input_with_test_word_get_list(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#the rules\naa;/a\naa r;aa /r\nsch;/s\nk ii;k /i\nii;/ie\nl;/l");
        tmp_rule_file.flush();

        self.assertEqual(False, os.stat(tmp_rule_file.name).st_size == 0);
        rule_manager = RuleManager(tmp_rule_file.name);
        rule_executor = RuleExecutor(rule_manager);

        input_word = (Token("aa"), Token("r"), Token("sch"), Token("l"), Token("ii"));

        # WHEN
        rule_manager.load_rules_from_file(tmp_rule_file.name);
        result = rule_executor.apply_transformation_rules_on_input(input_word);

        # THEN
        self.assertNotEqual(0, len(result));
        self.assertEqual(5, len(result));
        self.assertEqual("/a", result[0].get_value());
        self.assertEqual("/r", result[1].get_value());
        self.assertEqual("/s", result[2].get_value());
        self.assertEqual("/l", result[3].get_value());
        self.assertEqual("/ie", result[4].get_value());

if __name__ == '__main__':
    unittest.main()
