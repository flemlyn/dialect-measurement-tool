import unittest
import tempfile
from tokens import *

class TokenizerRuleLoadingTest(unittest.TestCase):

    def test_load_tokenization_rules_from_file_with_empty_file_get_empty_tuple(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();

        # WHEN
        try:
            tokenizer = Tokenizer(tmp_rule_file.name);

        # THEN
            self.assertEqual(0, len(tokenizer.get_available_token_rules()))

        finally:
            tmp_rule_file.close();

    def test_load_tokenization_rules_from_file_with_commented_file_get_empty_tuple(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#just a comment");
        tmp_rule_file.flush();

        # WHEN
        try:
            tokenizer = Tokenizer(tmp_rule_file.name);

        # THEN
            self.assertEqual(0, len(tokenizer.get_available_token_rules()))

        finally:
            tmp_rule_file.close();

    def test_load_tokenization_rules_from_file_with_file_get_tuple(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#just a comment\na\n");
        tmp_rule_file.flush();

        # WHEN
        try:
            tokenizer = Tokenizer(tmp_rule_file.name);
            first_token_rule = tokenizer.get_available_token_rules()[0];

        # THEN
            self.assertEqual(1, len(tokenizer.get_available_token_rules()))
            self.assertTrue(isinstance(tokenizer.get_available_token_rules()[0], TokenRule));
            self.assertEqual("a", first_token_rule.get_rule_value());

        finally:
            tmp_rule_file.close();

    def test_load_tokenization_rules_from_file_with_file_get_multiple_rules(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#just a comment\na\naa\nb");
        tmp_rule_file.flush();

        # WHEN
        try:
            tokenizer = Tokenizer(tmp_rule_file.name);
            first_token_rule = tokenizer.get_available_token_rules()[0];

        # THEN
            self.assertEqual(3, len(tokenizer.get_available_token_rules()))
            self.assertTrue(isinstance(tokenizer.get_available_token_rules()[0], TokenRule));
            self.assertEqual("aa", first_token_rule.get_rule_value());

        finally:
            tmp_rule_file.close();

    def test_load_tokenization_rules_from_file_with_file_get_special_rules(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#just a comment\n(aa)\nb");
        tmp_rule_file.flush();

        # WHEN
        try:
            tokenizer = Tokenizer(tmp_rule_file.name);
            first_token_rule = tokenizer.get_available_token_rules()[0];

        # THEN
            self.assertEqual(2, len(tokenizer.get_available_token_rules()))
            self.assertTrue(isinstance(tokenizer.get_available_token_rules()[0], TokenRule));
            self.assertEqual("(aa)", first_token_rule.get_rule_value());

        finally:
            tmp_rule_file.close();

    def test_load_tokenization_rules_from_file_with_file_get_sorted_rules(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#just a comment\na\nbb\n(ab)\n");
        tmp_rule_file.flush();

        # WHEN
        tokenizer = Tokenizer(tmp_rule_file.name);
        loaded_rules = tokenizer.get_available_token_rules();

        # THEN
        self.assertTrue(isinstance(loaded_rules[0], TokenRule));
        self.assertEqual("(ab)", (loaded_rules[0]).get_rule_value());
        self.assertEqual("bb", (loaded_rules[1]).get_rule_value());
        self.assertEqual("a", (loaded_rules[2]).get_rule_value());


class TokenizeWordTest(unittest.TestCase):

    def test_tokenize_word_with_rules_available_get_tokenized_word(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#token rules\na\nbb\n(ab)\nc\n");
        tmp_rule_file.flush();
        tokenizer = Tokenizer(tmp_rule_file.name);
        word_to_tokenize = "aabb(ab)abba(ab)";

        # WHEN
        tokens_list = tokenizer.tokenize_word(word_to_tokenize);

        # THEN
        self.assertTrue(isinstance(tokens_list[0], Token));
        self.assertEqual(8, len(tokens_list));
        self.assertEqual("a", tokens_list[0].get_value());
        self.assertEqual("a", tokens_list[1].get_value());
        self.assertEqual("bb", tokens_list[2].get_value());
        self.assertEqual("(ab)", tokens_list[3].get_value());
        self.assertEqual("a", tokens_list[4].get_value());
        self.assertEqual("bb", tokens_list[5].get_value());
        self.assertEqual("a", tokens_list[6].get_value());
        self.assertEqual("(ab)", tokens_list[7].get_value());

    def test_tokenize_word_with_missing_rules_get_error(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#token rules\nbb\n(ab)\n");
        tmp_rule_file.flush();
        tokenizer = Tokenizer(tmp_rule_file.name);
        word_to_tokenize = "aabb(ab)abba(ab)";

        # WHEN
        with self.assertRaises(NoTokenRuleFoundError) as context:
            tokenizer.tokenize_word(word_to_tokenize);

        # THEN

    def test_tokenize_word_with_rules_available_get_real_tokenized_word(self):
        # GIVEN
        tmp_rule_file = tempfile.NamedTemporaryFile();
        tmp_rule_file.write(b"#token rules\naa\nr\nsch\nl\nii\n");
        tmp_rule_file.flush();
        tokenizer = Tokenizer(tmp_rule_file.name);
        word_to_tokenize = "aarschlii";

        # WHEN
        tokens_list = tokenizer.tokenize_word(word_to_tokenize);

        # THEN
        self.assertTrue(isinstance(tokens_list[0], Token));
        self.assertEqual(5, len(tokens_list));
        self.assertEqual("aa", tokens_list[0].get_value());
        self.assertEqual("r", tokens_list[1].get_value());
        self.assertEqual("sch", tokens_list[2].get_value());
        self.assertEqual("l", tokens_list[3].get_value());
        self.assertEqual("ii", tokens_list[4].get_value());


class TokenTest(unittest.TestCase):

    def test_set_token_value(self):
        # GIVEN
        new_token = Token();

        # WHEN
        new_token.set_value("a");

        # THEN
        self.assertEqual("a", new_token.get_value());


if __name__ == '__main__':
    unittest.main()
