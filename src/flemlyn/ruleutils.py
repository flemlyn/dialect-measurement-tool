"""Module docstring

must be first, even before imports of the module

The docstring for a module should generally list the classes, exceptions and functions (and any other objects) that are exported by the module, with a one-line summary of each.
"""
from io import TextIOWrapper
import csv
import logging
from typing import List
from tokens import Token, TokenTuple, TokenList


class Rule:
    """This class represents a basic rule

    :ivar _premise: a tuple containing the tokens of the premise
    :ivar _conclusion: a tuple containing the tokens of the conclusion
    :ivar _premise_value: a token holding the changing value of the premise
    :ivar _conclusion_value: a token holding the changing value of the conclusion
    """

    def __init__(self, premise: TokenTuple, conclusion: TokenTuple):
        """Construct a rule with the given premise and conclusion

        The _premise_value and _conclusion_value fields contain an empty token,
        because the class of the rule is not defined yet. These values specify
        the part of the rule which is changed, e.g.: if the rule is 'a b c; a d c'
        then the value of _premise_value is Token('b') and the value of _conclusion_value
        is Token('d').

        :param premise: a tuple containing one to three Tokens
        :type premise: tuple
        :param conclusion: a tuple containing one to three Tokens
        :type conclusion: tuple
        """
        self._premise = premise;
        self._conclusion = conclusion;
        self._premise_value = Token();
        self._conclusion_value = Token();

    def get_premise(self):
        return self._premise;

    def set_premise(self, premise: tuple):
        self._premise = premise;

    def get_conclusion(self):
            return self._conclusion;

    def set_conclusion(self, conclusion: tuple):
        self._conclusion = conclusion;

    def get_premise_value(self):
            return self._premise_value;

    def set_premise_value(self, premise_value: str):
        self._premise_value = premise_value;

    def get_conclusion_value(self):
            return self._conclusion_value;

    def set_conclusion_value(self, conclusion_value: str):
        self._conclusion_value = conclusion_value;


class RuleC0 (Rule):
    """A rule of class 0"""

    def __init__(self, premise: tuple, conclusion: tuple):
        """Construct a rule with the given premise and conclusion

        :param premise: a tuple containing one Token
        :type premise: tuple
        :param conclusion: a tuple containing one Token
        :type conclusion: tuple
        """
        super().__init__(premise, conclusion);

        if self.__is_syntax_of_premise_correct() and self.__is_syntax_of_conclusion_correct():
            self._premise_value = premise[0];
            self._conclusion_value = conclusion[0];
        else:
            raise RuleSyntaxError("The premise and conclusion must be a tuple of length 1");

    def __is_syntax_of_premise_correct(self) -> bool:
        """Check if the syntax of the premise is correct

        The premise must be a tuple with length 1. That is, the tuple contains one Token.
        """
        if type(self._premise) is tuple and type(self._premise[0]) is Token and len(self._premise) == 1:
            return True;
        else:
            return False;

    def __is_syntax_of_conclusion_correct(self) -> bool:
        """Check if the syntax of the conclusion is correct

        The conclusion must be a tuple with length 1. That is, the tuple contains one Token.
        """
        if type(self._conclusion) is tuple and type(self._conclusion[0]) is Token and len(self._conclusion) == 1:
            return True;
        else:
            return False;


class RuleC1 (Rule):
    """A rule of class 1

    :ivar __context_before: a token holding the value of the context before the _premise_value
    """

    def __init__(self, premise: tuple, conclusion: tuple):
        """Construct a rule with the given premise and conclusion

        The first token of a tuple is taken as the context of the second token.

        :param premise: a tuple containing two Tokens
        :type premise: tuple
        :param conclusion: a tuple containing two Tokens
        :type conclusion: tuple
        """
        super().__init__(premise, conclusion);

        self.__context_before = Token();

        if self.__is_syntax_of_premise_correct() and self.__is_syntax_of_conclusion_correct():
            self._premise_value = premise[1];
            self._conclusion_value = conclusion[1];
        else:
            raise RuleSyntaxError();

        if premise[0].get_value() == conclusion[0].get_value():
            self.__context_before = premise[0];
        else:
            raise RuleSyntaxError("premise: " + premise[0].get_value() + "; conclusion: " + conclusion[0].get_value());


    def __is_syntax_of_premise_correct(self) -> bool:
        """Check if the syntax of the premise is correct

        The premise must be a tuple with length 2. That is, the tuple contains two Tokens.
        """
        if type(self._premise) is tuple and all(isinstance(x, Token) for x in self._premise) and len(self._premise) == 2:
            return True;
        else:
            return False;

    def __is_syntax_of_conclusion_correct(self) -> bool:
        """Check if the syntax of the conclusion is correct

        The conclusion must be a tuple with length 2. That is, the tuple contains two Tokens.
        """
        if type(self._conclusion) is tuple and all(isinstance(x, Token) for x in self._conclusion) and len(self._conclusion) == 2:
            return True;
        else:
            return False;

    def get_context_before(self):
            return self.__context_before;


class RuleC2 (Rule):
    """A rule of class 2

    :ivar __context_before: a token holding the value of the context before the _premise_value
    :ivar __context_after: a token holding the value of the context after the _premise_value
    """

    def __init__(self, premise: tuple, conclusion: tuple):
        """Construct a rule with the given premise and conclusion

        The first and third token of a tuple are taken as the context of the second token.

        :param premise: a tuple containing three Tokens
        :type premise: tuple
        :param conclusion: a tuple containing three Tokens
        :type conclusion: tuple
        """
        super().__init__(premise, conclusion);

        self.__context_before = Token();
        self.__context_after = Token();

        if self.__is_syntax_of_premise_correct() and self.__is_syntax_of_conclusion_correct():
            self._premise_value = premise[1];
            self._conclusion_value = conclusion[1];
        else:
            raise RuleSyntaxError();

        if premise[0].get_value() == conclusion[0].get_value() and premise[2].get_value() == conclusion[2].get_value():
            self.__context_before = premise[0];
            self.__context_after = premise[2];
        else:
            raise RuleSyntaxError();

    def __is_syntax_of_premise_correct(self) -> bool:
        """Check if the syntax of the premise is correct

        The premise must be a tuple with length 3. That is, the tuple contains three Tokens.
        """
        if type(self._premise) is tuple and all(isinstance(x, Token) for x in self._premise) and len(self._premise) == 3:
            return True;
        else:
            return False;

    def __is_syntax_of_conclusion_correct(self) -> bool:
        """Check if the syntax of the conclusion is correct

        The conclusion must be a tuple with length 3. That is, the tuple contains three Tokens.
        """
        if type(self._conclusion) is tuple and all(isinstance(x, Token) for x in self._premise) and len(self._conclusion) == 3:
            return True;
        else:
            return False;

    def get_context_before(self):
            return self.__context_before;

    def get_context_after(self):
        return self.__context_after;

# Type Aliases
RuleList = List[Rule];
RuleC0List = List[RuleC0];
RuleC1List = List[RuleC1];
RuleC2List = List[RuleC2];

class RuleManager:
    """This class represents the rule manager

    The rule manager is responsible for the management of the rules, like loading the rules,
    printing the rules etc.

    Methods
    -------

    get_rulesC0_list() -> RuleC0List
    get_rulesC1_list() -> RuleC1List
    get_rulesC2_list() -> RuleC2List
    load_rules_from_file(rule_file_path)
    get_all_rules_of_premise_value(premise_value) -> RuleList
    get_all_rules_of_conclusion_value(conclusion_value) -> RuleList
    check_if_rule_already_exists(rule) -> bool
    print_rules_of_class(class_identifier) -> str
    print_all_rules() -> str
    """

    def __init__(self, rule_file: TextIOWrapper):
        """Construct a rule manager"""
        self.__rule_file = rule_file;
        self.__rules_c0_list = [];
        self.__rules_c1_list = [];
        self.__rules_c2_list = [];

    def get_rulesC0_list(self) -> RuleC0List:
        return self.__rules_c0_list;

    def get_rulesC1_list(self) -> RuleC1List:
        return self.__rules_c1_list;

    def get_rulesC2_list(self) -> RuleC2List:
        return self.__rules_c2_list;

    def load_rules_from_file(self, rule_file: TextIOWrapper):
        """Load rules from a given csv file

        The rules are added to the corresponding rule lists depending on the class of the rules.
        The rules must have a specific format:

            a;a             -- class 0
            a B;a c         -- class 1
            a B c;a d c     -- class 2

        Be careful with empty spaces. Too many spaces may lead to a syntax error. Spaces are only
        allowed to separate the tokens.

        Lines starting with a # are interpreted as comments and are therefore skipped.

        :param rule_file: a csv file containing the rules
        :type rule_file: TextIOWrapper
        :raise RuleLoadingError: This exception indicates that a rule cannot be loaded
        from the given rule file
        """
        with open(rule_file) as loaded_file:
            csv_reader = csv.reader(loaded_file, delimiter=";");
            is_rule_loading_error = False;
            rule_loading_error_messages = '';

            for line in csv_reader:
                #print(line[0]);
                if not line or line[0].startswith("#"):
                    continue;
                # rule class 0
                if self.__is_rule_of_c0(line):
                    self.__add_rule_of_c0(line);
                # rule class 1
                elif self.__is_rule_of_c1(line):
                    self.__add_rule_of_c1(line);
                # rule class 2
                elif self.__is_rule_of_c2(line):
                    self.__add_rule_of_c2(line);
                else:
                    is_rule_loading_error = True;
                    message = "The rule \'" + line[0] + ";" + line[1] + "\' cannot be loaded. The problem could be a syntax error.";
                    logging.warning(message);
                    rule_loading_error_messages = rule_loading_error_messages + message + '\n';

            if is_rule_loading_error:
                raise RuleLoadingError(rule_loading_error_messages);

    def __is_rule_of_c0(self, rule: list) -> bool:
        """Check if the rule belongs to class 0

        Premise and conclusion must not contain an empty space.
        Premise and conclusion must have at least one character.

        :param rule: the rule to check
        :type rule: list
        :return: true if the rule belongs to class 0, otherwise false
        """
        if " " not in rule[0] and " " not in rule[1] and len(rule[0]) != 0 and len(rule[1]) != 0:
            return True;
        else:
            return False;

    def __is_rule_of_c1(self, rule: list) -> bool:
        """Check if the rule belongs to class 1

        Premise and conclusion must contain exactly one empty space.
        Premise and conclusion must have at least three characters.

        :param rule: the rule to check
        :type rule: list
        :return: true if the rule belongs to class 1, otherwise false
        """
        if rule[0].count(" ") == 1 and rule[1].count(" ") == 1 and len(rule[0]) >= 3 and len(rule[1]) >= 3:
            return True;
        else:
            return False;

    def __is_rule_of_c2(self, rule: list) -> bool:
        """Check if the rule belongs to class 2

        Premise and conclusion must contain exactly two empty spaces.
        Premise and conclusion must have at least five characters.

        :param rule: the rule to check
        :type rule: list
        :return: true if the rule belongs to class 2, otherwise false
        """
        if rule[0].count(" ") == 2 and rule[1].count(" ") == 2 and len(rule[0]) >= 5 and len(rule[1]) >= 5:
            return True;
        else:
            return False;

    def __add_rule_of_c0(self, line: list):
        """Add a rule of class 0 to the rules list"""
        logging.info("Adding a rule of class 0");
        premise = (Token(line[0]),);
        conclusion = (Token(line[1]),)
        new_rule = RuleC0(premise, conclusion);
        self.__rules_c0_list.append(new_rule);

    def __add_rule_of_c1(self, line: list):
        """Add a rule of class 1 to the rules list"""
        logging.info("Adding a rule of class 1");
        premise_context_before = Token(line[0].split(" ")[0]);
        premise = Token(line[0].split(" ")[1]);
        conclusion_context_before = Token(line[1].split(" ")[0]);
        conclusion = Token(line[1].split(" ")[1]);
        new_rule = RuleC1((premise_context_before, premise), (conclusion_context_before, conclusion));
        self.__rules_c1_list.append(new_rule);

    def __add_rule_of_c2(self, line: list):
        """Add a rule of class 2 to the rules list

        :param line: a list containing a line of a csv file
        :type line: list
        """
        logging.info("Adding a rule of class 2");
        splitted_premise = line[0].split();
        splitted_conclusion = line[1].split();

        premise_context_before = Token(splitted_premise[0]);
        premise_context_after = Token(splitted_premise[2]);
        premise = Token(splitted_premise[1]);
        conclusion_context_before = Token(splitted_conclusion[0]);
        conclusion_context_after = Token(splitted_conclusion[2]);
        conclusion = Token(splitted_conclusion[1]);
        new_rule = RuleC2((premise_context_before, premise, premise_context_after), (conclusion_context_before, conclusion, conclusion_context_after));
        self.__rules_c2_list.append(new_rule);

    def get_all_rules_with_premise_value(self, premise_value: Token) -> RuleList:
        """Get all rules which have the same premise value as the given one

        The rules are added in the following order: RuleC0, RuleC1, RuleC2.

        :param premise_value: the value of the premise to search
        :type premise_value: Token
        :return: a list containing all rules with the same premise value
        """
        rules_list = [] + self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC0_list());
        rules_list = rules_list + self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC1_list());
        rules_list = rules_list + self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC2_list());

        return rules_list;

    def get_all_rules_with_premise_value_from_list_c0(self, premise_value: Token) -> RuleC0List:
        """Get all rules of class 0 which have the same premise value as the given one

        :param premise_value: the value of the premise to search
        :type premise_value: Token
        :return: a list containing all rules of class 0 with the same premise value
        """
        return self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC0_list());

    def get_all_rules_with_premise_value_from_list_c1(self, premise_value: Token) -> RuleC1List:
        """Get all rules of class 1 which have the same premise value as the given one

        :param premise_value: the value of the premise to search
        :type premise_value: Token
        :return: a list containing all rules of class 1 with the same premise value
        """
        return self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC1_list());

    def get_all_rules_with_premise_value_from_list_c2(self, premise_value: Token) -> RuleC0List:
        """Get all rules of class 2 which have the same premise value as the given one

        :param premise_value: the value of the premise to search
        :type premise_value: Token
        :return: a list containing all rules of class 2 with the same premise value
        """
        return self.__get_all_rules_with_premise_value_from_list(premise_value, self.get_rulesC2_list());

    def __get_all_rules_with_premise_value_from_list(self, premise_value: Token, rules_list: RuleList) -> RuleList:
        """Get all rules which have the same premise value as the given one from the given list

        :param premise_value: the value of the premise to search
        :type premise_value: Token
        :param rules_list: the list of the searched rules
        :type rules_list: RuleList
        """
        found_rules_list = [];

        for rule in rules_list:
            if premise_value.get_value() == rule.get_premise_value().get_value():
                found_rules_list.append(rule);

        return found_rules_list;

    def get_all_rules_with_conclusion_value(self, conclusion_value: Token) -> RuleList:
        """Get all rules which have the same conclusion value as the given one

        The rules are added in the following order: RuleC0, RuleC1, RuleC2.

        :param conclusion_value: the value of the conclusion to search
        :type conclusion_value: Token
        """
        rules_list = [] + self.__get_all_rules_with_conclusion_value_from_list(conclusion_value, self.get_rulesC0_list());
        rules_list = rules_list + self.__get_all_rules_with_conclusion_value_from_list(conclusion_value, self.get_rulesC1_list());
        rules_list = rules_list + self.__get_all_rules_with_conclusion_value_from_list(conclusion_value, self.get_rulesC2_list());

        return rules_list;

    def __get_all_rules_with_conclusion_value_from_list(self, conclusion_value: Token, rules_list: RuleList) -> RuleList:
        """Get all rules which have the same conclusion value as the given one from the given list

        :param conclusion_value: the value of the conclusion to search
        :type conclusion_value: Token
        :param rules_list: the list of the searched rules
        :type rules_list: RuleList
        """
        found_rules_list = [];

        for rule in rules_list:
            if conclusion_value.get_value() == rule.get_conclusion_value().get_value():
                found_rules_list.append(rule);

        return found_rules_list;

    def check_if_rule_already_exists(self, rule: Rule) -> bool:
        """Check if the given rule already exists, that is, is already known to the rule manager

        :param rule: the rule to check
        :type rule: Rule
        :return: true if the given rule already exists, otherwise false
        :raise ClassNotFoundError: This exception indicates that the class of the given rule
        cannot be handled by the rule manager
        """
        if type(rule) == RuleC0:
            return self.__check_if_rule_exists_in_rule_list_c0(rule);
        elif type(rule) == RuleC1:
            return self.__check_if_rule_exists_in_rule_list_c1(rule);
        elif type(rule) == RuleC2:
            return self.__check_if_rule_exists_in_rule_list_c2(rule);
        else:
            raise ClassNotFoundError();

    def __check_if_rule_exists_in_rule_list_c0(self, rule: RuleC0) -> bool:
        """Check if the given rule exists in the rule list of class 0

        :param rule: the rule to check
        :type rule: RuleC0
        :return: true if the given rule exists in the list of class 0, otherwise false
        """
        rules_list = self.get_rulesC0_list();

        for existing_rule in rules_list:
            if rule.get_premise_value().get_value() == existing_rule.get_premise_value().get_value():
                if rule.get_conclusion_value().get_value() == existing_rule.get_conclusion_value().get_value():
                    return True;

        return False;

    def __check_if_rule_exists_in_rule_list_c1(self, rule: RuleC1) -> bool:
        """Check if the given rule exists in the rule list of class 1

        :param rule: the rule to check
        :type rule: RuleC1
        :return: true if the given rule exists in the list of class 1, otherwise false
        """
        rules_list = self.get_rulesC1_list();

        for existing_rule in rules_list:
            premise_value = existing_rule.get_premise_value().get_value();
            premise_context_before = existing_rule.get_context_before().get_value();
            conclusion_value = existing_rule.get_conclusion_value().get_value();

            if rule.get_premise_value().get_value() == premise_value:
                if rule.get_context_before().get_value() == premise_context_before:
                    if rule.get_conclusion_value().get_value() == conclusion_value:
                        return True;

        return False;

    def __check_if_rule_exists_in_rule_list_c2(self, rule: RuleC2) -> bool:
        """Check if the given rule exists in the rule list of class 2

        :param rule: the rule to check
        :type rule: RuleC2
        :return: true if the given rule exists in the list of class 2, otherwise false
        """
        rules_list = self.get_rulesC2_list();

        for existing_rule in rules_list:
            premise_value = existing_rule.get_premise_value().get_value();
            premise_context_before = existing_rule.get_context_before().get_value();
            premise_context_after = existing_rule.get_context_after().get_value();
            conclusion_value = existing_rule.get_conclusion_value().get_value();

            if rule.get_premise_value().get_value() == premise_value:
                if rule.get_context_before().get_value() == premise_context_before:
                    if rule.get_context_after().get_value() == premise_context_after:
                        if rule.get_conclusion_value().get_value() == conclusion_value:
                            return True;

        return False;

    def print_rules_of_class(self, class_identifier: int) -> str:
        """Print the rules of the class identified by the given class_identifier

        The output is printed to stdout and returned as a string.

        :param class_identifier: the identifier of the class
        :type class_identifier: int
        :return: the printed output
        """
        if class_identifier == 0:
            rules_list = self.get_rulesC0_list();
        elif class_identifier == 1:
            rules_list = self.get_rulesC1_list();
        elif class_identifier == 2:
            rules_list = self.get_rulesC2_list();
        else:
            raise ClassNotFoundError("The provided identifier is invalid. Available classes: 0,1,2");

        return self.__print_rules_of_any_class(rules_list);

    def __print_rules_of_any_class(self, rules_list: RuleList) -> str:
        """Print the rules of any class

        The output is printed to stdout and returned as a string.

        :param rules_list: the rules to print
        :type rules_list: RuleList
        :return: the formatted output or an info message if the rules list is empty
        :raise ClassIdentifierNotFoundError: Indicates that there is no print method for the rule class
        """
        if not rules_list:
            return "No rules to print\n";
        elif type(rules_list[0]) == RuleC0:
            return self.__print_c0_rules(rules_list);
        elif type(rules_list[0]) == RuleC1:
            return self.__print_c1_rules(rules_list);
        elif type(rules_list[0]) == RuleC2:
            return self.__print_c2_rules(rules_list);
        else:
            logging.error("The provided class is invalid: " + type(rules_list[0]));
            raise ClassNotFoundError();

    def __print_c0_rules(self, rules_list: RuleList):
        """Print the rules of class 0

        The output is printed to stdout and returned as a string.

        :param rules_list: the rules to print
        :type rules_list: RuleList
        :return: the formatted output
        """
        class0_string = "";
        for rule in rules_list:
            format_string = "class 0: premise: (" + rule.get_premise()[0].get_value() + ") -- conclusion: (" + rule.get_conclusion()[0].get_value() + ")\n";
            class0_string = class0_string + format_string;

        print(class0_string);
        return class0_string;

    def __print_c1_rules(self, rules_list: RuleList):
        """Print the rules of class 1

        The output is printed to stdout and returned as a string.

        :param rules_list: the rules to print
        :type rules_list: RuleList
        :return: the formatted output
        """
        class1_string = "";
        for rule in rules_list:
            premise_string = rule.get_premise()[0].get_value() + " " + rule.get_premise()[1].get_value();
            conclusion_string = rule.get_conclusion()[0].get_value() + " " + rule.get_conclusion()[1].get_value();
            format_string = "class 1: premise: (" + premise_string + ") -- conclusion: (" + conclusion_string + ")\n";
            class1_string = class1_string + format_string;

        print(class1_string);
        return class1_string;

    def __print_c2_rules(self, rules_list: RuleList) -> str:
        """Print the rules of class 2

        The output is printed to stdout and returned as a string.

        :param rules_list: the rules to print
        :type rules_list: RuleList
        :return: the formatted output
        """
        class2_string = "";
        for rule in rules_list:
            premise_string = rule.get_premise()[0].get_value() + " " + rule.get_premise()[1].get_value() + " " + rule.get_premise()[2].get_value();
            conclusion_string = rule.get_conclusion()[0].get_value() + " " + rule.get_conclusion()[1].get_value() + " " + rule.get_conclusion()[2].get_value();
            format_string = "class 2: premise: (" + premise_string + ") -- conclusion: (" + conclusion_string + ")\n";
            class2_string = class2_string + format_string;

        print(class2_string);
        return class2_string;

    def print_all_rules(self) -> str:
        """Print all rules

        The rules of all existing rule class lists are printed to stdout.

        :return: the printed output
        """
        output = [];
        output.append(self.print_rules_of_class(0));
        output.append(self.print_rules_of_class(1));
        output.append(self.print_rules_of_class(2));

        if output[0] == output[1] and output[1] == output[2]:
            return output[0];
        else:
            return output[0] + output[1] + output[2];


class RuleExecutor:
    """Class doc strings"""

    def __init__(self, rule_manager: RuleManager):
        """"""
        self.__rule_manager = rule_manager;

    def apply_transformation_rules_on_input(self, input_word: TokenList) -> TokenList:
        """Apply the transformation rules provided by the rule manager on the given input

        :param input_word: the input word the rules are applied on
        :type input_word: TokenList
        :return: the transformed word
        """
        if not input_word:
            logging.info("The given input word is empty.");
            return;
        elif type(input_word[0]) != Token:
            logging.error("The input word is not tokenized!");
            raise InputNotTokenizedError("The given input word is not tokenized.");
        else:
            return self.__apply_transformation_rules(input_word);

    def __apply_transformation_rules(self, input_word: TokenTuple) -> TokenList:
        """Apply rules on the given input word

        :param input_word: the word to transform
        :type input_word: TokenTuple
        :return: the transformed word
        """
        output_list = [];
        # apply rule of c0 on the word's first character
        transformed_token = self.__apply_rule_on_first_character(input_word[0]);
        output_list.append(transformed_token);
        # apply all rules on the next chars
        index = 1;
        while index < (len(input_word)-1):
            transformed_token = self.__apply_rule_on_mid_character((input_word[index - 1], input_word[index], input_word[index + 1]));
            output_list.append(transformed_token);
            index += 1;
        # apply rules on last char
        transformed_token = self.__apply_rule_on_last_character((input_word[index-1], input_word[index]));
        output_list.append(transformed_token);

        return output_list;

    def __apply_rule_on_first_character(self, first_character: Token) -> Token:
        """Apply a rule on the given first character

        :param first_character: the first character on which a rule is applied
        :type first_character: Token
        :return: the transformed token
        """
        return self.__apply_rule_of_c0_on_token(first_character);

    def __apply_rule_on_mid_character(self, tokens: TokenTuple) -> Token:
        """Apply all rules on the given mid character (premise_value)

        :param tokens: a tuple of length 3 containing the context_before, premise_value and context_after
        :type tokens: TokenTuple
        :return: the transformed token
        """
        if not tokens or len(tokens) != 3:
            raise Exception("The tokens tuple must not be empty and must have length 3.");

        #TODO: fix this block with chain pattern
        try:
            transformed_token = self.__apply_rule_of_c2_on_token(tokens);
        except NoRulesFoundError:
            try:
                transformed_token = self.__apply_rule_of_c1_on_token(tokens[0:2]);
            except NoRulesFoundError:
                transformed_token = self.__apply_rule_of_c0_on_token(tokens[1]);

        return transformed_token;

    def __apply_rule_on_last_character(self, tokens: TokenTuple) -> Token:
        """Apply a rule on the given last character (premise_value)

        :param tokens: a tuple of length 2 containing the context_before and premise_value
        :type tokens: Token
        :return: the transformed token
        """
        if not tokens or len(tokens) != 2:
            raise Exception("The tokens tuple must not be empty and must have length 2.");

        try:
            transformed_token = self.__apply_rule_of_c1_on_token(tokens);
        except NoRulesFoundError:
            transformed_token = self.__apply_rule_of_c0_on_token(tokens[1]);

        return transformed_token;

    def __apply_rule_of_c0_on_token(self, token: Token) -> Token:
        """Apply a rule of class 0 on the given token

        :param token: the token to apply a rule on
        :type token: Token
        :return: the transformed token
        """
        rules_to_apply = self.__rule_manager.get_all_rules_with_premise_value_from_list_c0(token);

        if not rules_to_apply:
            raise NoRulesFoundError("There are no rules c0 found for the premise value: " + token.get_value());
        elif len(rules_to_apply) > 1:
            raise MultipleRulesFoundError("There is more than one rule for the premise value: " + token.get_value());
        else:
            return rules_to_apply[0].get_conclusion_value();

    def __apply_rule_of_c1_on_token(self, tokens: TokenTuple) -> Token:
        """Apply rules of class 1 to the given token (premise_value)

        :param tokens: a tuple of length 2 containing the context_before and premise_value
        :type tokens: TokenTuple
        :return: the transformed token
        :raise NoRulesFoundError: if no rule for the given premise_value is known to the rule manager or if no rule can be applied
        """
        if not tokens or len(tokens) != 2:
            raise Exception("The tokens tuple must not be empty and must have length 2.");

        context_before = tokens[0];
        premise_value = tokens[1];

        rules_to_apply = self.__rule_manager.get_all_rules_with_premise_value_from_list_c1(premise_value);

        if not rules_to_apply:
            raise NoRulesFoundError("There are no rules c1 found for the premise value: " + premise_value.get_value());
        else:
            for rule in rules_to_apply:
                # TODO: currently if there are two rules, only one is applied: handle this!
                if rule.get_context_before().get_value() == context_before.get_value():
                    return rule.get_conclusion_value();

        logging.info("No rule was applied to: (" + context_before.get_value() + ", " + premise_value.get_value() + ")");
        raise NoRulesFoundError("There are no rules c1 applicable on the premise value: " + premise_value.get_value());

    def __apply_rule_of_c2_on_token(self, tokens: TokenTuple) -> Token:
        """Apply rules of class 2 to the given tokens

        :param tokens: a tuple of length 3 containing the context_before, premise_value and context_after
        :type tokens: TokenTuple
        :return: the transformed token
        :raise NoRulesFoundError: if no rule for the given premise_value is known to the rule manager or if no rule can be applied
        """
        if not tokens or len(tokens) != 3:
            raise Exception("The tokens tuple must not be empty and must have length 2.");

        context_before = tokens[0];
        premise_value = tokens[1];
        context_after = tokens[2];

        rules_to_apply = self.__rule_manager.get_all_rules_with_premise_value_from_list_c2(premise_value);

        if not rules_to_apply:
            raise NoRulesFoundError("There are no rules c2 found for the premise value: " + premise_value.get_value());
        else:
            for rule in rules_to_apply:
                # TODO: currently if there are two rules, only one is applied: handle this!
                if rule.get_context_before().get_value() == context_before.get_value():
                    if rule.get_context_after().get_value() == context_after.get_value():
                        return rule.get_conclusion_value();

        logging.info("No rule was applied to: (" + context_before.get_value() + ", " + premise_value.get_value() + ", " + context_after.get_value() + ")");
        raise NoRulesFoundError("There are no rules c2 applicable on the premise value: " + premise_value.get_value());

class RuleSyntaxError(Exception):
    """Class doc strings"""
    pass;

class ClassNotFoundError(Exception):
    """"""
    pass;

class InputNotTokenizedError(Exception):
    """"""
    pass;

class MultipleRulesFoundError(Exception):
    """"""
    pass;

class NoRulesFoundError(Exception):
    """"""
    pass;

class RuleLoadingError(Exception):
    """"""
    pass;