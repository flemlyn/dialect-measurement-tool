"""

"""
import Levenshtein as levenshtein
from typing import List, Tuple
from information_retrieval import InformationRetrievalManager

# Type Aliases
LevenshteinResultTuple = Tuple[str, str, int];
LevenshteinResultList = List[LevenshteinResultTuple];

class DistanceMeasuringManager:

    def __init__(self, info_retrieval_manager:InformationRetrievalManager=None):
        self.__info_retrieval_manager = info_retrieval_manager;

    def calculate_levenshtein_from_words(self, word1: str, word2: str) -> int:
        """"""
        return levenshtein.distance(word1, word2);

    def calculate_levenshtein_from_words_list(self, comp_word: str, word_list: list) -> LevenshteinResultList:
        """"""
        levensthein_result_list = [];
        for word in word_list:
            levensthein_result_list.append((comp_word, word, self.calculate_levenshtein_from_words(comp_word, word)));

        return levensthein_result_list;


    def __add_element_to_calculated_differences_list(self, new_result: tuple, calculated_differences: list) -> list:
        """Add a new result word to the calculated differences word list.

        A new word is added to the word list if the Levenshtein distance result of the new word
        is equal to the distance result value of the existing words. If the new word's distance
        value is smaller, the word list is cleaned and the new word is added.

        """
        for old_result in calculated_differences:
            if new_result[1] < old_result[1]:
                calculated_differences.clear();
                calculated_differences.append(new_result);
                return calculated_differences;
            elif new_result[1] == old_result[1]:
                calculated_differences.append(new_result);
                return calculated_differences;

        return calculated_differences;

    def get_least_differentiating_words_in_list(self, comp_word: str, word_list: list ) -> list:
        """Get all words from the given word list which least differ from the given comparison word.

        The Levenshtein distance is used to calculate the difference.
        """

        if not comp_word or not word_list:
            return [];

        calculated_differences = [];
        for word in word_list:
            result = (word, self.calculate_levenshtein_from_words(comp_word, word));

            if not calculated_differences:
                calculated_differences.append(result);
            else:
                calculated_differences = self.__add_element_to_calculated_differences_list(result, calculated_differences);

        return calculated_differences;

    def get_words_differing_less_than_value(self, diff_value: int, comp_word: str, word_list: list) -> list:
        """Get all words from the given list which differ less than the given diff value from the comparison word."""
        calculated_differences = self.get_least_differentiating_words_in_list(comp_word, word_list);

        if not calculated_differences:
            return [];
        else:
            result_list = [];

            for result in calculated_differences:
                if result[1] <= diff_value:
                    result_list.append(result);

            return result_list;

    def get_words_differing_less_than_half_of_word(self, comp_word: str, word_list: list) -> list:
        """"""
        maximum_diff_value = len(comp_word)/2;

        return self.get_words_differing_less_than_value(maximum_diff_value, comp_word, word_list);

    def __get_list_of_all_synonyms_for_word(self, dialect_word_tuple: tuple) -> list:
        """"""
        meanings_list = dialect_word_tuple[1:];
        print('\nMeanings from collection: ' + str(meanings_list));
        for meaning in dialect_word_tuple[1:]:
            word_synonyms_list = [];
            word_synonyms_list = word_synonyms_list + self.__info_retrieval_manager.query_synonyms_for_word(meaning);

        if not word_synonyms_list:
            print('Synonyms list is empty.')

        word_synonyms_list = word_synonyms_list + meanings_list;
        word_synonyms_list = list(dict.fromkeys(word_synonyms_list));

        print('Meanings including synonyms: ' + str(word_synonyms_list));

        return word_synonyms_list;

    def __get_ipa_for_standard_german_words(self, word_list: list) -> list:
        word_synonyms_ipa_list = [];

        for word in word_list:
            ipa_transcription = self.__info_retrieval_manager.get_ipa_data_for_standard_german_word(word);
            if ipa_transcription:
                word_synonyms_ipa_list.append(ipa_transcription);
            else:
                print('No IPA found on wiktionary for: ' + word);

        return word_synonyms_ipa_list;

    def get_distance_measuring_result(self, dialect_word_tuple: tuple) -> LevenshteinResultList:
        """"""
        word_synonyms_list = self.__get_list_of_all_synonyms_for_word(dialect_word_tuple);

        # get ipa of synonyms
        word_synonyms_ipa_list = self.__get_ipa_for_standard_german_words(word_synonyms_list);

        # calculate distance
        return self.calculate_levenshtein_from_words_list(dialect_word_tuple[0], word_synonyms_ipa_list);