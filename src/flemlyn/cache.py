import csv

class WiktionaryCache:

    def __init__(self, cache_filename:str='wiktionary_cache.csv'):
        self.__cache_file = cache_filename;
        self.__delimiter = ',';
        self.__cache = {};

        self.__load_cache_from_file();

    def __load_cache_from_file(self):
        with open(self.__cache_file, 'r') as cache_file:
            csv_cache_file_reader = csv.reader(cache_file, delimiter=self.__delimiter);

            for row in csv_cache_file_reader:
                if row[0].startswith("#") or row[0].isspace():
                    continue;

                self.__cache.update({row[0] : row[1]});

    def is_element_in_cache(self, element: str) -> bool:
        if element in self.__cache:
            print('Cache hit for: ' + element);
            return True;
        else:
            return False;

    def __write_element_to_cache_file(self, key: str, value: str):
        with open(self.__cache_file, 'a') as cache_file:
            csv_cache_file_writer = csv.writer(cache_file, delimiter=self.__delimiter);

            row = (key, value);
            csv_cache_file_writer.writerow(row);

    def add_element_to_cache(self, key: str, value:str):
        self.__cache.update({key : value});
        self.__write_element_to_cache_file(key, value);


    def get_value_from_cache(self, key: str) -> str:
        if self.__cache[key]:
            return self.__cache[key];
        else:
            return None;