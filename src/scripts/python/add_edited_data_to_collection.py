#! /bin/python

import csv
#TODO: -help -dry (for dry mode: only print data, dont add it)
import sys

csv_delimiter= ';';

# Get file paths from the user
print("\nConsider that the input file must be encoded with UTF-8!\n");
print("You can comment out lines with a preceding #.\nThese lines will not be considered when processing.\n");
print("###############\n");
working_directory=input("Please name the path of the working directory:\n");
csv_collection_file=input("Please provide the name of the collection file where the data should be added:\n");
csv_phrases_file=input("Please provide the name of the input file whose data should be added to the collection:\n");

# prepare the working directory and files
if(working_directory and not working_directory.endswith("/")):
    working_directory = working_directory + "/";

csv_collection_file= working_directory + csv_collection_file;
csv_phrases_file= working_directory + csv_phrases_file;

# add data from the input file to the collection file
with open(csv_collection_file, "a") as collection_file, open(csv_phrases_file, "r") as input_file:
    write_collection_csv = csv.writer(collection_file, delimiter=csv_delimiter);
    read_input_csv = csv.reader(input_file, delimiter=csv_delimiter);
    counter_moved_datasets = 0;

    for row in read_input_csv:
        if (row[0].startswith("#")):
            continue;
        elif(row[0].startswith("!")):
            row[0] = row[0][1:];
            write_collection_csv.writerow(row);
            counter_moved_datasets += 1;


print("\nAmount of moved datasets: " + str(counter_moved_datasets));