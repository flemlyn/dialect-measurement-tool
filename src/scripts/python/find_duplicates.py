#! /bin/python

import csv
import sys

csv_delimiter= ',';

# Get file paths from the user
print("\nConsider that the input file must be encoded with UTF-8!\n");
print("You can comment out lines with a preceding #.\nThese lines will not be considered when processing.\n");
print("###############\n");
working_directory=input("Please name the path of the working directory:\n");
csv_collection_file=input("Please provide the name of the collection file where duplicates should be found:\n");

# prepare the working directory and files
if(working_directory and not working_directory.endswith("/")):
    working_directory = working_directory + "/";

csv_collection_file= working_directory + csv_collection_file;

# add data from the input file to the collection file
with open(csv_collection_file, "r") as collection_file:
    read_collection_csv = csv.reader(collection_file, delimiter=csv_delimiter);
    found_duplicates = 0;
    duplicates_list = [];

    print("\n#### Duplicates ####\n");
    for row in read_collection_csv:
        if (row[0].startswith("#")):
            continue;
        else:
            if row[0] in duplicates_list:
                print(row[0]);
                found_duplicates += 1;
            else:
                duplicates_list.append(row[0])

print("\nAmount of found duplicates: " + str(found_duplicates));