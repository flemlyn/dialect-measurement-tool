#! /bin/python

import csv
#TODO: -help -dry (for dry mode: only print data, dont add it)
import sys

csvDelimiter=';';

# Get file paths from the user
print("\nConsider that the input file must be encoded with UTF-8!\n");
print("You can comment out lines with a preceding #.\nThese lines will not be considered when processing.\n");
print("###############\n");
working_directory=input("Please name the path of the working directory:\n");
csvCollectionFile=input("Please provide the name of the collection file:\n");
csvRemovedPhrasesFile=input("Please provide the name of the output file:\n");

# prepare the working directory and files
if(working_directory and not working_directory.endswith("/")):
    working_directory = working_directory + "/";

csvCollectionFile=working_directory + csvCollectionFile;
csvRemovedPhrasesFile=working_directory + csvRemovedPhrasesFile;

# create the output name of the edited input file
index_of_file_ending = csvCollectionFile.find(".csv");
if(index_of_file_ending != -1):
    csvEditedCollectionFile=csvCollectionFile;
    csvEditedCollectionFile=csvEditedCollectionFile[:index_of_file_ending] + "_edited" + csvEditedCollectionFile[index_of_file_ending:];
else:
    csvEditedCollectionFile = working_directory + "edited_collection.csv";

# remove phrases from the input file
with open(csvCollectionFile, "r") as inputFile, open(csvRemovedPhrasesFile, "w") as outputFileWithPhrases, open(csvEditedCollectionFile, "w") as outputFile:
    readCSV = csv.reader(inputFile, delimiter=csvDelimiter);
    writePhrasesCsv = csv.writer(outputFileWithPhrases, delimiter=csvDelimiter);
    writeCollectionCsv = csv.writer(outputFile, delimiter=csvDelimiter);
    counter_moved_datasets = 0;

    for row in readCSV:
        if (row[0].startswith("#")):
            continue;

        is_found = False;
        for item in row:
            if(' ' in item):
                writePhrasesCsv.writerow(row);
                counter_moved_datasets += 1;
                is_found = True;
                break;

        if (not is_found):
            writeCollectionCsv.writerow(row);

print("\nAmount of moved datasets: " + str(counter_moved_datasets));