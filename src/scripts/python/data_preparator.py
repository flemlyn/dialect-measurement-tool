class DataPreparator:

    """Information about the class, see PEP 257.

    More Info.
    """

    def __is_file_encoded_with_utf8(input_file) -> bool:
        """Check if a given file is encoded with UTF-8.

        Return true if the file is UTF-8 encoded.

        Parameters:

        input_file -- the input file to check
        """
        return False;


    def check_encoding_of_an_input_file(input_file):
        """Check the encoding of a given input file.

        Parameters:

        input_file -- the input file to check
        """
        if(not __is_file_encoded_with_utf8(input_file)):
            #TODO: throw some exception;
            return;



    def convert_a_file_to_utf8(inputFile):
        """Convert a given input file to UTF-8.

        Parameters:

        input_file -- the input file to convert
        """